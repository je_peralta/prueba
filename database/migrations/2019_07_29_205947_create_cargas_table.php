<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCargasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('SEMESTRE')->nullable();
            $table->string('CARNET')->nullable();
            $table->string('APELLIDOS')->nullable();
            $table->string('NOMBRES')->nullable();
            $table->string('NACIONALIDAD')->nullable();
            $table->string('PROGRAMA')->nullable();
            $table->string('DOBLE PROG')->nullable();
            $table->string('ENFASIS')->nullable();
            $table->string('DOC IDENT')->nullable();
            $table->string('FECHA NAC')->nullable();
            $table->string('SEXO')->nullable();
            $table->string('DIRECCION ESTUD')->nullable();
            $table->string('CIUDAD')->nullable();
            $table->string('AREA_TEL')->nullable();
            $table->string('TELEFONO')->nullable();
            $table->string('EXTENSION')->nullable();
            $table->string('AREA_CELULAR')->nullable();
            $table->string('CELULAR')->nullable();
            $table->string('SIT ACAD')->nullable();
            $table->string('CRED TOMADOS')->nullable();
            $table->string('CRED APROB')->nullable();
            $table->string('CRED PGA')->nullable();
            $table->string('PROM ACUM')->nullable();
            $table->string('CRED TRANSF')->nullable();
            $table->string('ULT SEM CURS')->nullable();
            $table->string('CRED SEM TOMADOS')->nullable();
            $table->string('CRED SEM APROB')->nullable();
            $table->string('CRED SEM PGA')->nullable();
            $table->string('PROM SEM')->nullable();
            $table->string('SSC')->nullable();
            $table->string('EMAIL')->nullable();
            $table->string('CRED SEM ACTUAL')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargas');
    }
}
