<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasillerosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casilleros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('LOGIN')->nullable();
            $table->string('CELULAR')->nullable();
            $table->string('NOMBRE_EST')->nullable();
            $table->string('APELLIDO_EST')->nullable();
            $table->string('CORREO')->nullable();
            $table->string('DOC_IDENT')->nullable();
            $table->string('CORREO_ALTERNO')->nullable();            
            $table->string('STREET_LINE')->nullable();
            $table->string('ESTADO')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('casilleros');
    }
}
