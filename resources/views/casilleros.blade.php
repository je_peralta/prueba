@foreach($casilleros as $casillero)


<?php

$nombre = $casillero->NOMBRE_EST." ".$casillero->APELLIDO_EST;

?>

INSERT INTO USERINFO
(
"LOGIN",
@if($casillero->CELULAR) 
MOBILE,
@endif
"NAME",
@if($casillero->CORREO)
MAIL,
@endif
@if($casillero->DOC_IDENT)
IDENTIFICATION,
@endif
@if($casillero->CORREO_ALTERNO)
"USER",
@endif
@if($casillero->STREET_LINE)
ADDRESS,
@endif 
STATUS
)
VALUES
(
'{{$casillero->LOGIN}}', 
@if($casillero->CELULAR)
'{{$casillero->CELULAR}}',
@endif
 
'{{$nombre}}',
 
@if($casillero->CORREO)
'{{$casillero->CORREO}}',
@endif
@if($casillero->DOC_IDENT)
'{{$casillero->DOC_IDENT}}',
@endif
@if($casillero->CORREO_ALTERNO)
'{{$casillero->CORREO_ALTERNO}}',
@endif
@if($casillero->STREET_LINE)
'{{$casillero->STREET_LINE}}',
@endif
1);
<br>

 

@endforeach 


<?php
/*
UPDATE USERINFO
SET 
@if($casillero->CELULAR)
	MOBILE  = '{{$casillero->CELULAR}}',
@endif
@if($casillero->CORREO_ALTERNO)
    "USER"  = '{{$casillero->CORREO_ALTERNO}}',
@endif   
@if($casillero->STREET_LINE) 
    ADDRESS = '{{$casillero->STREET_LINE}}', 
@endif     
    STATUS  = 1
WHERE LOGIN = '{{$casillero->LOGIN}}';
<br>
*/



 
